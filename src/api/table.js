import request from "@/utils/request";

export function getList(params) {
  return request({
    url: "/vue-admin-template/table/list",
    method: "get",
    params,
  });
}

export function getCardsList(params) {
  return request({
    url: "/wsp/getCardsList",
    method: "get",
    params,
  });
}
