# vue-admin-template

## Setup

```bash


# install dependency
npm install

# develop
npm start
```

This will automatically open http://localhost:9528

## Build

```bash

# build for production environment
npm run build
```
